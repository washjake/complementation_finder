import os, sys
import sqlite3

#input: Combined_report.txt, Bonf/Benj-Hoch, P-value cutoff/False discovery rate, genotype file

SeqCount_cutoff=44 #number of aligned seqs required for inclusion.
report=open(sys.argv[1],"rU")
test=sys.argv[2]
sig=float(sys.argv[3])

#read report values into dictionary
gene_pos={} #dictionary with combined gene ID and codon number as index. Value is P-vale.
for line in report:
	split=line.split("\n")[0].split("\t")
	if split[0]=="GeneID": continue
	if len(split) != 13: continue #skip rows that did not output correctly
	try: int(split[8])
	except ValueError: continue
	if int(split[8]) < SeqCount_cutoff: continue
	if split[10]=="": 
		print "no reference AA. May indicate stop codon issues with BAD_MUTATION analysis/CDS input files."
		continue
	gene_pos[split[0]+"@"+str(split[1])]=float(split[7])
	#print split[0],split[1],split[7],split[8]
report.close()

if test[:4]=="Bonf":
	print "Performing Bonferroni multipule test correction"
	#Determine Bonferroni corrected P-value
	print "Number of independent tests: "+str(len(gene_pos))
	print "User specified P-value cutoff: "+str(sig)
	cor_p=sig/len(gene_pos)
	print "Bonferroni corrected P-value cutoff: "+str(cor_p)
	
	#Output tests passing threshold.
	#for test in gene_pos:
	#	if gene_pos[test]<cor_p:
	#		print test, gene_pos[test]

if test[:9]=="Benj-Hoch":
	print "Performing Benjamini-Hochberg multiple test correction. NOT YET IMPLEMENTED"
	db = sqlite3.connect(':memory:')
	quit()

#read in genotype file and output list of significant genes matching file.
geno_file=open(sys.argv[4])
sig_genes=[]
for line in geno_file:
	split=line.split("\n")[0].split("\t")
	if split[0]=="CHROM": continue
	#print split[6].upper()+"@"+split[7][1:-1]
	if split[6].upper()+"@"+split[7][1:-1] in gene_pos:
		#print split[6]
		if split[6] not in sig_genes:
			sig_genes.append(split[6])
geno_file.close()

outfile=open(sys.argv[4].split("_")[0]+"_BAD_MUTATIONS"+"_seqcut"+str(SeqCount_cutoff)+"_"+test+"_p"+str(sig)+".txt","w")
for x in sorted(sig_genes):
	outfile.write(x+"\n")
outfile.close()
print len(sig_genes)