
# coding: utf-8

# In[1]:

import os, sys
import pandas as pd


# In[2]:

wrk_dir = "../../Zm_APGv4_to_Sb_orthos/"
in_files = [x for x in os.listdir(wrk_dir) if x[-13:] == "_inersect.txt"]


# In[3]:

#infile = in_files[1]
for infile in in_files:
    print infile
    #ortholog_file=open(wrk_dir+"S.b_1to1_Z.mV4.txt", "rU") #open(sys.argv[1],"rU")
    ortholog_file=open(wrk_dir+"S.b_all_Z.mV4.txt", "rU") #open(sys.argv[1],"rU")
    #input: List from CoGE, gene list for matching and translation
    gene_file=open("../../Zm_APGv4_to_Sb_orthos/"+infile,"rU") #open(sys.argv[2],"rU")
    gene_list=[]
    for line in gene_file:
        if line=="\n":continue
        if line.split("\n")[0] not in gene_list:
            gene_list.append(line.split("\n")[0])
    print ("Found "+str(len(gene_list))+" genes in "+sys.argv[2]+".")
    #print gene_list
    
    out_list=[]
    for line in ortholog_file:
        if line[:1]=="#": continue
        in_gene=line.split("\t")[5].split("||")[3].split("_T")[0].split("CDS:")[1]
        ortho=line.split("\t")[1].split("||")[3].split(".")[0]+"."+line.split("\t")[1].split("||")[3].split(".")[1]
        #print(ortho+"\t"+in_gene)
        if in_gene in gene_list: out_list.append(ortho) #out.write(ortho+"\n") #print(ortho+"\t"+in_gene)
    out_list = pd.DataFrame(list(set(out_list)))
    print len(out_list)
    out_list.to_csv(wrk_dir+infile+"_Sb_syn_orthologs",header=False, index=False)
    ortholog_file.close()
    gene_file.close()


# In[ ]:



