
# coding: utf-8

# In[1]:

#This script takes four lists of genes (one for each genotype being evaluated) which
#SnpEff (or other software) has determined to be deleterious. Each list should begin with
#a genotype name or identifier followed by "_".  For example A188_snpeff.txt. Those lists
#must be entered in the command line in the desired order for checking complementation. 
#Along with those lists it also requires four matching lists which must start with the same
#identifier (for example A188_ ) and end with "count.txt".  These lists contain all of 
#the genes of that genotype which mapped onto the reference genome, and must be present in
#same directory as the first lists.

#Both lists can be generated using SnpEff and SnpSift with the following commands:
#java -jar -Xmx10g snpEff.jar count <REFERENCE GENOME> A188_Ph207.sam > A188_Ph207_count.txt"
#cat A188_Ph207_snpeff.vcf | scripts/vcfEffOnePerLine.pl | java -jar SnpSift.jar filter "(exists LOF[*].PERC) & (LOF[*].PERC > 0.5) & ( EFF[*].IMPACT = 'HIGH' )" | java -jar /SnpSift.jar extractFields - "EFF[0].GENE" > A188_snpeff.txt

import os, sys


# In[ ]:

type="Gene"
read_cutoff=50

#read in mapped genes lists and place each in a non-redundant dictionary with the value "FUNCTIONAL".
count={}
infile={}
MAPPED={}
for x in range(1,5):
	for file in os.listdir("."):
		if file[:len(sys.argv[x].split("_")[0])]==sys.argv[x].split("_")[0] and file[-9:] =="count.txt": count[x]=file
	infile[x] = open(count[x], "rU")
	MAPPED[x]={}
	for line in infile[x]:
		if line=="\n":continue
		if line.split("\t")[0]=="chr": continue
		if line.split("\t")[3].find(type)==-1 : continue
		if int(line.split("\t")[4].split("\n")[0]) < read_cutoff: continue
		MAPPED[x][line.split("\t")[3][line.split("\t")[3].find(type):].split(":")[1]]="FUNCTIONAL"
	infile[x].close()
	#print MAPPED[x]
#print "\n"


# In[ ]:

#read in lists of "deleterious" genes. For each gene in the list, change the corresponding
#genotype dictionary value for the corresponding gene to "NON-FUNCTIONAL".
infile={}
for x in range(1,5):
	infile[x] = open(sys.argv[x],"rU")
	for line in infile[x]:
		if line.split("\n")[0]=="EFF[0].GENE": continue
		gene=line.split("\n")[0]
		if gene in MAPPED[x]: MAPPED[x][gene]="NON-FUNCTIONAL"
	infile[x].close()
	#print MAPPED[x]


# In[ ]:

#print total number of mapped genes for each genotype and total number of "non-functional"
#genes for each. Also write functional genes into new lists for every taxon.
stats=open(sys.argv[1].split("_")[0]+"_"+sys.argv[2].split("_")[0]+"_"+sys.argv[3].split("_")[0]+"_"+sys.argv[4]+"_stats.txt","w")
print ("Total mapped genes in:")
stats.write("Total mapped genes in:\n")
for x in range(1,5):
	print (sys.argv[x].split("_")[0]+": "+str(len(MAPPED[x])))
	stats.write(sys.argv[x].split("_")[0]+": "+str(len(MAPPED[x]))+"\n")
mapt1t2=str(len(list(set(MAPPED[1]) | set(MAPPED[2]))))
print ("Union "+sys.argv[1].split("_")[0]+"/"+sys.argv[2].split("_")[0]+": "+mapt1t2)
stats.write("Union "+sys.argv[1].split("_")[0]+"/"+sys.argv[2].split("_")[0]+": "+mapt1t2+"\n")
mapt3t4=str(len(list(set(MAPPED[3]) | set(MAPPED[4]))))
print ("Union "+sys.argv[3].split("_")[0]+"/"+sys.argv[4].split("_")[0]+": "+mapt3t4)
stats.write("Union "+sys.argv[3].split("_")[0]+"/"+sys.argv[4].split("_")[0]+": "+mapt3t4+"\n")
mappedb1b2b3b4=str(len(list(set(MAPPED[1]) | set(MAPPED[2]) | set(MAPPED[3]) | set(MAPPED[4]))))
print ("Union all mapped genes: "+mappedb1b2b3b4)
stats.write("Union all mapped genes: "+mappedb1b2b3b4+"\n")
Taxon1=[]
Taxon2=[]
Taxon3=[]
Taxon4=[]
print ("Total predicted FUNCTIONAL genes in:")
stats.write("Total predicted FUNCTIONAL genes in:\n")
for x in range(1,5):
	count=0
	for gene1 in MAPPED[x]:
		if MAPPED[x][gene1]=="FUNCTIONAL":
			if x==1: Taxon1.append(gene1)
			if x==2: Taxon2.append(gene1)
			if x==3: Taxon3.append(gene1)
			if x==4: Taxon4.append(gene1)
			count+=1
	print (sys.argv[x].split("_")[0]+": "+str(count))
	stats.write(sys.argv[x].split("_")[0]+": "+str(count)+"\n")
t1t2=str(len(list(set(Taxon1) | set(Taxon2))))
print (sys.argv[1].split("_")[0]+"/"+sys.argv[2].split("_")[0]+": "+t1t2)
stats.write(sys.argv[1].split("_")[0]+"/"+sys.argv[2].split("_")[0]+": "+t1t2+"\n")
t3t4=str(len(list(set(Taxon3) | set(Taxon4))))
print (sys.argv[3].split("_")[0]+"/"+sys.argv[4].split("_")[0]+": "+t3t4)
stats.write(sys.argv[3].split("_")[0]+"/"+sys.argv[4].split("_")[0]+": "+t3t4+"\n")
t1t2t3t4=str(len(list(set(Taxon1) | set(Taxon2) | set(Taxon3) | set(Taxon4))))
print (sys.argv[1].split("_")[0]+"/"+sys.argv[2].split("_")[0]+"/"+sys.argv[3].split("_")[0]+"/"+sys.argv[4].split("_")[0]+": "+t1t2t3t4)
stats.write (sys.argv[1].split("_")[0]+"/"+sys.argv[2].split("_")[0]+"/"+sys.argv[3].split("_")[0]+"/"+sys.argv[4].split("_")[0]+": "+t1t2t3t4+"\n")
#print Taxon1,Taxon2,Taxon3,Taxon4
#print len(Taxon1),len(Taxon2),len(Taxon3),len(Taxon4)
print ("Total predicted NON-FUNCTIONAL genes in:")
stats.write("Total predicted NON-FUNCTIONAL genes in:\n")
for x in range(1,5):
	count=0
	for gene1 in MAPPED[x]:
		if MAPPED[x][gene1]=="NON-FUNCTIONAL":
			count+=1
	print (sys.argv[x].split("_")[0]+": "+str(count))
	stats.write(sys.argv[x].split("_")[0]+": "+str(count)+"\n")
Complemented_in_single_cross1=[val for val in Taxon1 if val not in Taxon2]+[val for val in Taxon2 if val not in Taxon1]
Complemented_in_single_cross2=[val for val in Taxon3 if val not in Taxon4]+[val for val in Taxon4 if val not in Taxon3]
print ("Complemented in "+sys.argv[1].split("_")[0]+"x"+sys.argv[2].split("_")[0]+": "+str(len(Complemented_in_single_cross1)))
print ("Complemented in "+sys.argv[3].split("_")[0]+"x"+sys.argv[4].split("_")[0]+": "+str(len(Complemented_in_single_cross2)))
stats.write("Complemented in "+sys.argv[1].split("_")[0]+"x"+sys.argv[2].split("_")[0]+": "+str(len(Complemented_in_single_cross1))+"\n")
stats.write("Complemented in "+sys.argv[3].split("_")[0]+"x"+sys.argv[4].split("_")[0]+": "+str(len(Complemented_in_single_cross2))+"\n")

single_cross1_homo_dominant=[val for val in Taxon1 if val in Taxon2]
single_cross2_homo_dominant=[val for val in Taxon3 if val in Taxon4]

Double_cross_complemented_from_single_cross1=[val for val in single_cross1_homo_dominant if val not in Taxon3+Taxon4]
Double_cross_complemented_from_single_cross2=[val for val in single_cross2_homo_dominant if val not in Taxon1+Taxon2]
Double_cross_complemented=Double_cross_complemented_from_single_cross1 + Double_cross_complemented_from_single_cross2
#print len(Double_cross_complemented_from_single_cross1)
#print len(Double_cross_complemented_from_single_cross2)
print ("Complemented in "+sys.argv[1].split("_")[0]+"/"+sys.argv[2].split("_")[0]+"/"+sys.argv[3].split("_")[0]+"/"+sys.argv[4].split("_")[0]+": "+str(len(Double_cross_complemented)))
stats.write("Complemented in "+sys.argv[1].split("_")[0]+"/"+sys.argv[2].split("_")[0]+"/"+sys.argv[3].split("_")[0]+"/"+sys.argv[4].split("_")[0]+": "+str(len(Double_cross_complemented))+"\n")

Complements_from_SC1_reducing_in_DC=[val for val in Complemented_in_single_cross1 if val not in single_cross2_homo_dominant]
Complements_from_SC2_reducing_in_DC=[val for val in Complemented_in_single_cross2 if val not in single_cross1_homo_dominant]
Genes_reducing_double_cross_heterosis=Complements_from_SC1_reducing_in_DC+Complements_from_SC2_reducing_in_DC
print ("Genes Complemented in "+sys.argv[1].split("_")[0]+"x"+sys.argv[2].split("_")[0]+" but Potentially Reducing "+sys.argv[1].split("_")[0]+"/"+sys.argv[2].split("_")[0]+"/"+sys.argv[3].split("_")[0]+"/"+sys.argv[4].split("_")[0]+" heterosis: "+str(len(Complements_from_SC1_reducing_in_DC)))
print ("Genes Complemented in "+sys.argv[3].split("_")[0]+"x"+sys.argv[4].split("_")[0]+" but Potentially Reducing "+sys.argv[1].split("_")[0]+"/"+sys.argv[2].split("_")[0]+"/"+sys.argv[3].split("_")[0]+"/"+sys.argv[4].split("_")[0]+" heterosis: "+str(len(Complements_from_SC2_reducing_in_DC)))
print ("Genes Complemented in single crosses but Potentially Reducing "+sys.argv[1].split("_")[0]+"/"+sys.argv[2].split("_")[0]+"/"+sys.argv[3].split("_")[0]+"/"+sys.argv[4].split("_")[0]+" heterosis: "+str(len(Genes_reducing_double_cross_heterosis)))
stats.write("Genes Complemented in "+sys.argv[1].split("_")[0]+"x"+sys.argv[2].split("_")[0]+" but Potentially Reducing "+sys.argv[1].split("_")[0]+"/"+sys.argv[2].split("_")[0]+"/"+sys.argv[3].split("_")[0]+"/"+sys.argv[4].split("_")[0]+" heterosis: "+str(len(Complements_from_SC1_reducing_in_DC))+"\n")
stats.write("Genes Complemented in "+sys.argv[3].split("_")[0]+"x"+sys.argv[4].split("_")[0]+" but Potentially Reducing "+sys.argv[1].split("_")[0]+"/"+sys.argv[2].split("_")[0]+"/"+sys.argv[3].split("_")[0]+"/"+sys.argv[4].split("_")[0]+" heterosis: "+str(len(Complements_from_SC2_reducing_in_DC))+"\n")
stats.write("Genes Complemented in single crosses but Potentially Reducing "+sys.argv[1].split("_")[0]+"/"+sys.argv[2].split("_")[0]+"/"+sys.argv[3].split("_")[0]+"/"+sys.argv[4].split("_")[0]+" heterosis: "+str(len(Genes_reducing_double_cross_heterosis))+"\n")

print "Writing genes to files"
DC_genes_out=open(sys.argv[1].split("_")[0]+"_"+sys.argv[2].split("_")[0]+"_"+sys.argv[3].split("_")[0]+"_"+sys.argv[4]+"_DC_genes.txt","w")
DC_genes_out.writelines(["%s\n" % item for item in Double_cross_complemented])

SC_complements_reducing_DC=open(sys.argv[1].split("_")[0]+"_"+sys.argv[2].split("_")[0]+"_"+sys.argv[3].split("_")[0]+"_"+sys.argv[4]+"_SC_complements_reducing_DC.txt","w")
SC_complements_reducing_DC.writelines(["%s\n" % item for item in Genes_reducing_double_cross_heterosis])

